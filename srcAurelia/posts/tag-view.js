import { inject } from 'aurelia-framework';
import { PostsService } from '../posts/posts-service';

@inject(PostsService)
export class TagView {
  constructor(PostsService) {
    this.postsService = PostsService;
  }

  activate(params) {
    this.tag = params.tag;
    this.postsService.postsByTag(this.tag).then(data => {
      this.posts = data.posts;
    }).catch(error => {
      this.error = error.message;
    });
  }
}
