import { inject } from 'aurelia-framework';
import { PostsService } from '../posts/posts-service';

@inject(PostsService)
export class ArchiveView {
  constructor(PostsService) {
    this.postsService = PostsService;
  }

  activate(params) {
    this.archive = params.archive;
    this.postsService.postsByArchive(this.archive).then(data => {
      this.posts = data.posts;
    }).catch(error => {
      this.error = error.message;
    });
  }
}
