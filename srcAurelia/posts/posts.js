import { inject } from 'aurelia-framework';
import { PostsService } from '../posts/posts-service';

@inject(PostsService)
export class Posts {

    constructor(PostsService) {
        this.postsService = PostsService;
    }

    attached() {
        this.error = '';
        this.postsService.allPostPreviews().then(data => {
            this.posts = data.posts;
            console.log(this.posts);
        }).catch(error => {
            this.error = error.message;
        });
    }
}