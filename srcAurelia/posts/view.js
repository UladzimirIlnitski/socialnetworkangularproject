import { inject } from 'aurelia-framework';
import { PostsService } from '../posts/posts-service';

@inject(PostsService)
export class View {

  constructor(PostsService) {
    this.postsService = PostsService;
  }

  activate(params) {
    this.error = '';
    this.postsService.find(params.slug).then(data => {
      this.post = data.post;
    }).catch(error => {
      this.error = error.message;
    }
    );
  }
}
