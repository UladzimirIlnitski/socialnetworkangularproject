export class PostsService {
    constructor() {
        this.dalay = 100;

        if (!this.posts) {
            this.posts = [
                {
                    title: 'First post',
                    body: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi, modi.',
                    slug: 'first-post',
                    tags: ['aurelia', 'lorem', 'javascript'],
                    createdAt: new Date('July 1, 2019'),
                },
                {
                    title: 'Second post',
                    body: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ad, quidem ducimus?',
                    slug: 'second-post',
                    tags: ['lorem', 'javascript'],
                    createdAt: new Date('August 17, 2019'),
                    // createdAt: new Date('December 2, 2019'),
                },
                {
                    title: 'Third post',
                    body: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi quaerat illo consectetur in assumenda blanditiis.',
                    slug: 'third-post',
                    tags: ['aurelia', 'javascript'],
                    createdAt: new Date('December 1, 2019'),
                }
            ]
        }
    }

    allPostPreviews() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (this.posts) {
                    const priviews = this.posts.map(post => {
                        return {
                            title: post.title,
                            body: post.body.substring(0, 20) + '...',
                            slug: post.slug,
                            tags: post.tags,
                            createdAt: post.createdAt
                        }
                    })
                    priviews.sort((a, b) => b.createdAt - a.createdAt);

                    resolve({ posts: priviews });
                } else {
                    reject(new Error('There was an error retrieving the posts.'));
                }
            }, this.dalay);
        })
    }

    allArchives() {
        let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const archives = [];
                this.posts.sort((a, b) => b.createdAt - a.createdAt);
                this.posts.forEach(post => {
                    archives.push(`${months[post.createdAt.getMonth()]} ${post.createdAt.getFullYear()}`);
                });
                if (archives) {
                    resolve({ archives: archives.filter((v, i, a) => a.indexOf(v) === i) })
                } else {
                    reject(new Error('There was an error retrieving the archives.'));
                }
            }, this.dalay);
        })
    }

    allTags() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                let tags = [];
                this.posts.forEach(post => {
                    tags = tags.concat(post.tags);
                });
                if (tags) {
                    resolve({ tags: tags.filter((v, i, a) => a.indexOf(v) === i) });
                } else {
                    reject(new Error('There was an error retrieving the tags.'));
                }
            }, this.delay);
        })
    }

    create(post) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const slug = this.slugify(post.title);
                this.posts.push({
                    title: post.title,
                    body: post.body,
                    slug,
                    tags: post.tags,
                    createdAt: new Date()
                });
                resolve({ slug })
            }, this.delay);
        })
    }

    find(slug) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const post = this.posts.find(post => post.slug.toLowerCase() === slug);
                if (post) {
                    resolve({ post });
                } else {
                    reject(new Error('Post not found.'));
                }
            }, this.delay);
        })
    }

    postsByTag(tag) {  
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (!this.posts) {
                    reject(new Error('Error finding posts.'));
                } else {
                    resolve({ posts: this.posts.filter(post => post.tags.includes(tag)).sort((a, b) => b.createdAt - a.createdAt) });
                }
            }, this.delay);
        });
    }

    postsByArchive(archive) {
        let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (!this.posts) {
                    reject(new Error('Error finding posts.'));
                } else {
                    resolve({
                        posts: this.posts.filter(post => {
                            return archive === `${months[post.createdAt.getMonth()]} ${post.createdAt.getFullYear()}`;
                        }).sort((a, b) => b.createdAt - a.createdAt)
                    })
                }
            }, this.delay);
        });
    }

    slugify(text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')
            .replace(/[^\w\-]+/g, '')
            .replace(/\-\-+/g, '-')
            .replace(/^-+/, '')
            .replace(/-+$/, '')
    }

    update(post) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                let toUpdate = this.posts.find(x => {
                    return x.slug === post.slug;
                })
                if (!toUpdate) {
                    reject(new Error('There was an error updating the post'));
                } else {
                    toUpdate = post;
                    resolve({ slug: toUpdate.slug });
                }
            }, this.delay);
        });
    }
}