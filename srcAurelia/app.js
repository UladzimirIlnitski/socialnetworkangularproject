import { PLATFORM } from "aurelia-pal";
import { inject } from 'aurelia-framework';
import { PostsService } from './posts/posts-service';

@inject(PostsService)
export class App {
  constructor(PostsService) {
    this.postsService = PostsService;
    this.message = 'Hello World!';
  }

  attached() {
    this.postsService.allTags().then(data => {
      this.tags = data.tags;
    }).catch(error => {
      this.error = error.message;
    });
    this.postsService.allArchives().then(data => {
      this.archives = data.archives;
    }).catch(error => {
      this.error = error.message;
    });
  }

  configureRouter(config, router) {
    config.title = "some title";
    config.map([
      { route: '', name: 'home', moduleId: PLATFORM.moduleName('posts/posts'), title: 'All post' },
      { route: 'post/:slug', name: 'post-view', moduleId: PLATFORM.moduleName('posts/view'), title: 'View Post' },
      { route: 'tag/:tag', name: 'tag-view', moduleId: PLATFORM.moduleName('posts/tag-view'), title: 'View Posts by Tag' },
      { route: 'archive/:archive', name: 'archive-view', moduleId: PLATFORM.moduleName('posts/archive-view'), title: 'View Posts by Archive' }
    ]);
  }

}
