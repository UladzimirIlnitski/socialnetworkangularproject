export class Vk {


    constructor() {
        this.mesaga = "megomessaga"
        this.user;

        window.vkAsyncInit = function () {
            VK.init({
                apiId: 7276688
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://vk.com/js/api/openapi.js?162";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'vk-jssdk'));
    }

    login() {
        VK.Auth.login((response) => {
            if (response.session) {
                console.log(response);
            } else {
                console.error("User login failed");
            }
        });
    }

    logout() {
        VK.Auth.logout((response) => {
            if (response.session === null) {
                console.log(response);
                console.log(this.user);
                this.user.photo_100 = "";
                this.user.last_name = "";
                this.user.first_name = "";
            }
        });
    }

    getInfo() {
        VK.api(`users.get`, { fields: "photo_100,online", v: "5.103" }, (data) => {
            if (data.response) {
                this.user = data['response'][0];
                console.log(this.user);

            } else {
                if (data.error) {
                    console.error(data.error.error_msg);
                } else {
                    console.error(data);
                }
            }
        });
    }
}