import { Component, OnInit, ChangeDetectorRef, Host } from '@angular/core';
import { AppComponent } from '../app.component';
declare var VK: any;

@Component({
  selector: 'app-vkontakte',
  templateUrl: './vkontakte.component.html',
  styleUrls: ['./vkontakte.component.css']
})
export class VkontakteComponent implements OnInit {
  firstname: string = "";
  lastname: string = "";
  srcAvatar: string = "";
  online = 0;
  user: any;
  session: any;
  posts: [];
  friends: [];
  time = 0;

  constructor(private changeDetectorRef: ChangeDetectorRef, @Host() private parent: AppComponent) { }

  ngOnInit() {
    (window as any).vkAsyncInit = function () {
      VK.init({
        apiId: 7276688
      });
    };

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://vk.com/js/api/openapi.js?162";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'vk-jssdk'));

    if (localStorage.getItem("TOKEN_NAME") === "VK") {
      (window as any).onload = () => {
        this.login();
      };
    }
  }

  login() {
    VK.Auth.login((response) => {
      if (response.session) {
        this.session = response.session;
        localStorage.setItem("TOKEN_NAME", "VK");
        this.parent.social = "VK";
        // console.log(response);    
      } else {
        console.error("User login failed");
      }
    });
  }

  logout() {
    VK.Auth.logout(function (response) {
      if (response.session === null) {
        // console.log(response);
      }
    });
    this.clearInfo();
    localStorage.removeItem("TOKEN_NAME");
    this.parent.social = "All";
  }

  getInfo() {
    if (this.session) {
      if (this.time + 7200000 < new Date().getTime()) {
        this.getBaseInfo();
        this.getWall();
        this.getFriends();
      } else {
        this.getCache();
      }
    }
  }

  setBaseData(user: any) {
    this.online = user.online;
    this.srcAvatar = user['photo_100'];
    this.firstname = user.first_name;
    this.lastname = user.last_name;
  }

  clearInfo() {
    this.srcAvatar = "";
    this.firstname = "";
    this.lastname = "";
    this.posts = [];
    this.friends = [];
    this.session = null;
  }

  getBaseInfo() {
    VK.api(`users.get`, { fields: "photo_100,online", v: "5.103" }, (data) => {
      if (data.response) {
        this.user = data['response'][0];
        console.log(this.user);
        this.setBaseData(this.user);
        this.time = new Date().getTime();
        localStorage.setItem("me", JSON.stringify(this.user));
      } else {
        if (data.error) {
          console.error(data.error.error_msg);
        } else {
          console.error(data);
        }
      }
    });
  }

  getWall() {
    VK.api(`wall.get`, { v: "5.103" }, (data) => {
      if (data.response) {
        this.posts = data['response'].items;
        // console.log(this.posts);
        localStorage.setItem("wall", JSON.stringify(this.posts));
        this.changeDetectorRef.detectChanges();
      } else {
        if (data.error) {
          console.error(data.error.error_msg);
        } else {
          console.error(data);
        }
      }
    });
  }

  getFriends() {
    VK.api(`friends.get`, { fields: "photo_50,online", v: "5.103" }, (data) => {
      if (data.response) {
        this.friends = data['response'].items;
        // console.log(this.friends);
        localStorage.setItem("friends", JSON.stringify(this.friends));
        this.changeDetectorRef.detectChanges();
      } else {
        if (data.error) {
          console.error(data.error.error_msg);
        } else {
          console.error(data);
        }
      }
    });
  }

  getCache() {
    console.log("get cache");
    this.setBaseData(JSON.parse(localStorage.getItem("me")));
    this.friends = JSON.parse(localStorage.getItem("friends"));
    this.posts = JSON.parse(localStorage.getItem("wall"));
  }
}
