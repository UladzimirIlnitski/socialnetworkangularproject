import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  session: any;
  version: string = "5.103";
  constructor(private http: HttpClient) { }

  getUserVk() {
    if (this.loadSession()) {
      const user_ids = this.session.session.mid;
      const fields = "bdate,photo_100,online";
      const access_token = this.session.session.sid;

      return this.http.jsonp(`https://api.vk.com/method/users.get?user_ids=${user_ids}&fields=${fields}&access_token=${access_token}&v=${this.version}`, 'callback')
    }
    return new Observable;
  }

  getWallVK() {
    this.loadSession()
    const user_ids = this.session.session.mid;
    const access_token = this.session.session.sid;

    return this.http.jsonp(`https://api.vk.com/method/wall.get?user_ids=${user_ids}&access_token=${access_token}&v=${this.version}`, 'callback')
  }

  getFriendsVK() {
    const user_ids = this.session.session.mid;
    const access_token = this.session.session.sid;
    const fields = "photo_50,online";

    return this.http.jsonp(`https://api.vk.com/method/friends.get?user_ids=${user_ids}&fields=${fields}&access_token=${access_token}&v=${this.version}&name_case=nom`, 'callback')
  }

  loadSession() {
    this.session = JSON.parse(localStorage.getItem("TOKEN_NAME"));
    if (this.session) {
      return true;
    }
    return false;
  }
}


// getInfo() {
  // this.userService.getUserVk()
  //   .subscribe(data => {
  //     if (data !== undefined) {
  //       // console.log(data['response'][0]);
  //       this.user = data['response'][0];
  //       this.setImage(this.user['photo_100']);
  //       this.firstname = this.user.first_name;
  //       this.lastname = this.user.last_name;
  //       this.getWall();
  //       this.getFriends();
  //     }
  //   });
// }

// getWall() {
  // this.userService.getWallVK()
  //   .subscribe(
  //     data => {
  //       this.posts = data['response'].items;
  //       console.log(this.posts);
  //     }
  //   )
// }

// getFriends() {
  //   this.userService.getFriendsVK()
  //     .subscribe(
  //       data => {
  //         this.friends = data['response'].items;
  //         console.log(data);
  //       }
  //     )
// }

// status() {
  // VK.Auth.getLoginStatus(function (response) {
  //   console.log(response);
  //   localStorage.setItem("TOKEN_NAME", response);
  // });
// }