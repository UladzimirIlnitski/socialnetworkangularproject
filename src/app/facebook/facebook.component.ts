import { Component, OnInit, Host } from '@angular/core';
import { AppComponent } from '../app.component';
declare var FB: any;

@Component({
  selector: 'app-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.css']
})
export class FacebookComponent implements OnInit {
  firstName: string = "";
  srcAvatar: string = "";
  user: any;
  posts: [];
  friends = 0;
  time = 0;

  constructor(@Host() private parent: AppComponent) { }

  ngOnInit() {
    (window as any).fbAsyncInit = function () {
      FB.init({
        appId: '583652515806117',
        cookie: true,
        xfbml: true,
        version: 'v5.0'
      })
      FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    if (localStorage.getItem("TOKEN_NAME") === "FB") {
      (window as any).onload = () => {
        this.loginFacebook();
      };
    }
  }


  loginFacebook() {
    FB.login((response: any) => {
      if (response.authResponse) {
        localStorage.setItem("TOKEN_NAME", "FB");
        this.user = response.authResponse;
        this.parent.social = "FB";
        // console.log(this.user);
      }
      else {
        console.error('User login failed');
      }
    }, { scope: 'public_profile, user_friends, user_posts' });
  }

  logoutFacebook() {
    if (this.user) {
      FB.logout((response: any) => {
        // console.log(response);
        this.clearInfo();
        this.parent.social = "All";
        localStorage.removeItem("TOKEN_NAME");
      })
    }
  }

  clearInfo() {
    this.user = null;
    this.firstName = "";
    this.srcAvatar = "";
    this.posts = [];
    this.friends = 0;
  }

  getInfo() {
    if (this.user) {
      if (this.time + 7200000 < new Date().getTime()) {
        this.getBaseInfo();
        this.getFriends();
        this.getPosts();
        this.time = new Date().getTime();
      } else {
        this.getCache();
      }
    }
  }

  getBaseInfo() {
    FB.api(
      `/me`, { fields: 'name, picture' },
      (response: any) => {
        if (response && !response.error) {
          this.firstName = response['name'];
          this.srcAvatar = response['picture']['data']['url'];
          localStorage.setItem("me", JSON.stringify(response));
          console.log(response);
        } else {
          if (response.error) {
            console.error(response.error.message);
          } else {
            console.error(response);
          }
        }
      }
    );
  }

  getFriends() {
    FB.api(
      `/me/friends`,
      (response: any) => {
        if (response && !response.error) {
          // console.log(response);
          this.friends = response['summary']['total_count'];
          localStorage.setItem("friends", response['summary']['total_count']);
        } else {
          if (response.error) {
            console.error(response.error.message);
          } else {
            console.error(response);
          }
        }
      }
    );
  }

  getPosts() {
    FB.api(
      `/me/feed`,
      (response: any) => {
        if (response && !response.error) {
          // console.log(response);
          this.posts = response['data'];
          localStorage.setItem("feed", JSON.stringify(response['data']));
        } else {
          if (response.error) {
            console.error(response.error.message);
          } else {
            console.error(response);
          }
        }
      }
    );
  }

  getCache() {
    const baseData = JSON.parse(localStorage.getItem("me"));
    this.firstName = baseData['name'];
    this.srcAvatar = baseData['picture']['data']['url'];
    this.friends = +localStorage.getItem("friends");
    this.posts = JSON.parse(localStorage.getItem("feed"));
    console.log("get cache");
  }
}
