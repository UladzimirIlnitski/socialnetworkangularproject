import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class BasicInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler) {
        console.log("otlov");
        // request = request.clone({
        //     setHeaders: {
        //         // "Access-Control-Allow-Origin": "*",
        //         "Access-Control-Allow-Origin": '*',
        //         "Content-Type": '123'
        //         // Access-Control-Allow-Methods: 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
        //         // "Access-Control-Allow-Headers": 'Origin, Content-Type, X-Auth-Token'
        //         // Auto: "123"
        //     }
        // })
        const clonedRequest = request.clone({ headers: request.headers.set('Origin', '*') });

        return next.handle(clonedRequest);
    }
}