import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClientJsonpModule } from '@angular/common/http';
import { BasicInterceptor } from './helper/basic.interceptor';
import { FacebookComponent } from './facebook/facebook.component';
import { VkontakteComponent } from './vkontakte/vkontakte.component';

@NgModule({
  declarations: [
    AppComponent,
    FacebookComponent,
    VkontakteComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: BasicInterceptor, multi: true }],

  bootstrap: [AppComponent]
})
export class AppModule { }
